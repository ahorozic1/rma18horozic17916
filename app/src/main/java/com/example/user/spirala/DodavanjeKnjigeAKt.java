package com.example.user.spirala;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DodavanjeKnjigeAKt extends AppCompatActivity {
    private static Integer RESULT_LOAD_IMAGE=100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodavanje_knjige_akt);

        Button dugmePonisti=(Button)findViewById(R.id.dPonisti);
        Button dugmeUpisiKnjigu=(Button) findViewById(R.id.dUpisiKnjigu) ;
        Button dugmeNadjiSliku=(Button) findViewById(R.id.dNadjiSliku);
        EditText nazivAutora=(EditText) findViewById(R.id.imeAutora);
        EditText nazivKnjige=(EditText)findViewById(R.id.nazivKnjige);
        Spinner kategorija=(Spinner) findViewById(R.id.sKategorijaKnjige);
        ImageView slika=(ImageView) findViewById(R.id.naslovnaStr);


        //dodavanje u spiner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, KategorijeAkt.kategorije );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        kategorija.setAdapter(adapter);


        dugmeNadjiSliku.setEnabled(false);
        //na dugme da se spasi knjiga
        dugmeUpisiKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText nazivAutora=(EditText) findViewById(R.id.imeAutora);
                EditText nazivKnjige=(EditText)findViewById(R.id.nazivKnjige);
                Button dugmeNadjiSliku=(Button) findViewById(R.id.dNadjiSliku);
                Spinner kategorija=(Spinner) findViewById(R.id.sKategorijaKnjige);

                KategorijeAkt.knjige.add(0, new Knjiga(nazivKnjige.getText().toString(),nazivKnjige.getText().toString(),nazivAutora.getText().toString(),kategorija.getSelectedItem().toString() ));
                nazivAutora.setText("");
                nazivKnjige.setText("");
                dugmeNadjiSliku.setEnabled(false);
                kategorija.setSelection(0);


            }
        });

        //da se ne moze odabrati slika ako prije ne unese ime knjige
         nazivKnjige.addTextChangedListener(new TextWatcher() {
             @Override
             public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

             }

             @Override
             public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                 EditText nazivKnjige=(EditText)findViewById(R.id.nazivKnjige);
                 Button dugmeNadjiSliku=(Button) findViewById(R.id.dNadjiSliku);
                 if(nazivKnjige.toString().trim().length()==0){
                     dugmeNadjiSliku.setEnabled(false);
                 } else {
                     dugmeNadjiSliku.setEnabled(true);
                 }

             }

             @Override
             public void afterTextChanged(Editable editable) {

             }
         });


         //dugmeponisti da vrati na pocetnu
        dugmePonisti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        finish();
            }


        });

        //dugme nadi sliku da biras sliku
        dugmeNadjiSliku.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("image/*");
                if (i.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(i, RESULT_LOAD_IMAGE);
                }

            }
        });
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {

            ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
            parcelFileDescriptor.close();
            return image;

    }

    @Override
    protected void onActivityResult(int requestcode,int resultcode, Intent data) {
        if(resultcode==RESULT_CANCELED)
        {
            // action cancelled
        }
        if(resultcode==RESULT_OK) {
            EditText nazivKnjige=(EditText) findViewById(R.id.nazivKnjige);
            Uri slikaOdabrana = data.getData();
            try {
                FileOutputStream outputStream;
                outputStream = openFileOutput(nazivKnjige.getText().toString(), Context.MODE_PRIVATE);
                try {
                    getBitmapFromUri(data.getData()).compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
                    outputStream.close();
                } catch (IOException ex) {
                    Logger.getLogger(DodavanjeKnjigeAKt.class.getName()).log(Level.SEVERE, null, ex);
                }

                ImageView slika = (ImageView) findViewById(R.id.naslovnaStr);
                slika.setImageBitmap(BitmapFactory.decodeStream(openFileInput(nazivKnjige.getText().toString())));

            } catch (FileNotFoundException e) {
                Logger.getLogger(DodavanjeKnjigeAKt.class.getName()).log(Level.SEVERE, null, e);
            }
        }



    }



}

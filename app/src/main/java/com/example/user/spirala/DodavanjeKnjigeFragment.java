package com.example.user.spirala;
//import android.support.v4.app.Fragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class DodavanjeKnjigeFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {
        //Button pokategorijama=(Button)findViewById(R.layout.d)
        return inflater.inflate(R.layout.fragment_dodavanje_knjige, container, false);

    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //Button pokategorijama=(Button)getActivity().findViewById(R.lay);



            Button dugmePonisti=(Button)getView().findViewById(R.id.dPonisti);
            Button dugmeUpisiKnjigu=(Button)getView().findViewById(R.id.dUpisiKnjigu) ;
            Button dugmeNadjiSliku=(Button)getView().findViewById(R.id.dNadjiSliku);
            EditText nazivAutora=(EditText)getView().findViewById(R.id.imeAutora);
            EditText nazivKnjige=(EditText)getView().findViewById(R.id.nazivKnjige);
            Spinner kategorija=(Spinner)getView().findViewById(R.id.sKategorijaKnjige);
            ImageView slika=(ImageView)getView().findViewById(R.id.naslovnaStr);

            //dodavanje u spiner
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, KategorijeAkt.kategorije );
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            kategorija.setAdapter(adapter);


            dugmeNadjiSliku.setEnabled(false);
            //na dugme da se spasi knjiga
            dugmeUpisiKnjigu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EditText nazivAutora=(EditText)getView().findViewById(R.id.imeAutora);
                    EditText nazivKnjige=(EditText)getView().findViewById(R.id.nazivKnjige);
                    Button dugmeNadjiSliku=(Button)getView().findViewById(R.id.dNadjiSliku);
                    Spinner kategorija=(Spinner)getView().findViewById(R.id.sKategorijaKnjige);
                    String uneseniautor=nazivAutora.getText().toString();
                    boolean ima=false;
                    for(int i=0; i<KategorijeAkt.autori.size(); i++){
                        if(KategorijeAkt.autori.get(i).getImeiPrezime().equals(uneseniautor)){
                            ima=true;
                            KategorijeAkt.autori.get(i).setBrojKnjiga(KategorijeAkt.autori.get(i).getBrojKnjiga()+1);
                        }
                    }
                    if(!ima){
                        KategorijeAkt.autori.add(new Autor(uneseniautor));
                    }
                    Knjiga nova=new Knjiga(nazivKnjige.getText().toString(),nazivKnjige.getText().toString(),nazivAutora.getText().toString(),kategorija.getSelectedItem().toString() );
                    KategorijeAkt.knjige.add(0,nova) ;
                    KategorijeAkt.boh.dodajKnjigu(nova);
                    //FragmentOnline.dodajKnjigu(nova);
                    nazivAutora.setText("");
                    nazivKnjige.setText("");
                    dugmeNadjiSliku.setEnabled(false);
                    kategorija.setSelection(0);


                }
            });

            //da se ne moze odabrati slika ako prije ne unese ime knjige
            nazivKnjige.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    EditText nazivKnjige=(EditText)getView().findViewById(R.id.nazivKnjige);
                    Button dugmeNadjiSliku=(Button)getView().findViewById(R.id.dNadjiSliku);
                    if(nazivKnjige.toString().trim().length()==0){
                        dugmeNadjiSliku.setEnabled(false);
                    } else {
                        dugmeNadjiSliku.setEnabled(true);
                    }

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


            //dugmeponisti da vrati na pocetnu
            dugmePonisti.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentManager fm= getFragmentManager();
                    fm.popBackStack();

                    }



            });

            //dugme nadi sliku da biras sliku
            dugmeNadjiSliku.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Integer RESULT_LOAD_IMAGE=100;
                    Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                    i.setType("image/*");
                    if (i.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivityForResult(i, RESULT_LOAD_IMAGE);
                    }

                }
            });
        }

        private Bitmap getBitmapFromUri(Uri uri) throws IOException {

            ParcelFileDescriptor parcelFileDescriptor = getActivity().getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
            parcelFileDescriptor.close();
            return image;

        }

        @Override
        public void onActivityResult(int requestcode, int resultcode, Intent data) {
            if(resultcode==RESULT_CANCELED)
            {
                // action cancelled
            }
            if(resultcode==RESULT_OK) {
                EditText nazivKnjige=(EditText)getView().findViewById(R.id.nazivKnjige);
                Uri slikaOdabrana = data.getData();
                try {
                    FileOutputStream outputStream;
                    outputStream =getActivity().openFileOutput(nazivKnjige.getText().toString(), Context.MODE_PRIVATE);
                    try {
                        getBitmapFromUri(data.getData()).compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
                        outputStream.close();
                    } catch (IOException ex) {
                        Logger.getLogger(DodavanjeKnjigeAKt.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    ImageView slika = (ImageView)getView().findViewById(R.id.naslovnaStr);
                    slika.setImageBitmap(BitmapFactory.decodeStream(getActivity().openFileInput(nazivKnjige.getText().toString())));

                } catch (FileNotFoundException e) {
                    Logger.getLogger(DodavanjeKnjigeAKt.class.getName()).log(Level.SEVERE, null, e);
                }
            }

    }
}

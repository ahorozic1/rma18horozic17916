package com.example.user.spirala;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdapterZaAutore extends ArrayAdapter<Autor> {
    private Context mContext;
    private ArrayList<Knjiga> listaKnjiga;

    public AdapterZaAutore(Context context, ArrayList<Autor> k) {
        super(context, 0, k);
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        //View v=View.inflate(mContext, R.layout.element_liste, null);
        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.element_liste_autor, viewGroup, false);
        }
        TextView tekstzaimeiprezime = (TextView) view.findViewById(R.id.tImeiPrezime);
        //TextView tekstzaprezime = (TextView) view.findViewById(R.id.tPrezimeAutora);
        TextView tekstzabrojknjiga = (TextView) view.findViewById(R.id.tBrojKnjiga);
        //postavljanje

        tekstzaimeiprezime.setText("    Autor: "+getItem(i).getImeiPrezime());
        //tekstzabrojknjiga.setText("     Broj knjiga: "+ String.valueOf((getItem(i).getBrojKnjiga())));

        // v.setTag(listaKnjiga.get(i).getId());

        return view;

    }
}

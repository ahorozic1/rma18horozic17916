package com.example.user.spirala;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class KnjigeFragment extends Fragment  {
    static public  ArrayList<Knjiga> odabraneknjige;

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_lista_knjige, container, false);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        Autor ovogautoratrazis=null;
        String ovukategorijutrazis = null;
        odabraneknjige = new ArrayList<Knjiga>();





        if(getArguments().containsKey("poslanakategorija")) {
            ListView lista = (ListView) getView().findViewById(R.id.listaKnjiga);

            ovukategorijutrazis = getArguments().getString("poslanakategorija");
            String id=KategorijeAkt.boh.dajIdKategorije(ovukategorijutrazis);

           /* for (int i = 0 ; i < KategorijeAkt.knjige.size(); i++) {
                if (KategorijeAkt.knjige.get(i).getKategorija().equals(ovukategorijutrazis))
                    odabraneknjige.add(KategorijeAkt.knjige.get(i));
            }*/

            odabraneknjige=KategorijeAkt.boh.knjigeKategorije(Long.parseLong(id));

            AdapterZaKnjige adaptercic = new AdapterZaKnjige(getActivity(), odabraneknjige, new AdapterZaKnjige.OnChangeFragmentListener(){
                @Override
                public void changeFragment(int position) {
                    Bundle argumenti=new Bundle();
                    argumenti.putParcelable("knjiga", KnjigeFragment.odabraneknjige.get(position));
                    android.app.FragmentManager fm = getFragmentManager();
                    FragmentPreporuci fp;
                    fp=new FragmentPreporuci();
                    fp.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.fFragmenti, fp).addToBackStack(null).commit();
                }
            });

            /*

                    KnjigeFragment kf;
                    //kf = (KnjigeFragment) fm.findFragmentById(R.id.fFragmenti);
                    //provjerimo da li je fragment već kreiran
                    //kreiramo novi fragment FragmentDetalji ukoliko već nije kreiran
                    kf = new KnjigeFragment();

                    Bundle argumenti = new Bundle();
                    argumenti.putParcelable("poslaniautor", autori.get(position));
                    kf.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.fFragmenti, kf).addToBackStack(null).commit();
             */

            lista.setAdapter(adaptercic);

            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    view.setBackgroundColor(getActivity().getResources().getColor(R.color.bojaZaElementLise));
                    odabraneknjige.get(position).setDaLiJeObojena(true);
                    for (Knjiga k: KategorijeAkt.knjige) {
                        if(k.getNaziv().equals(odabraneknjige.get(position).getNaziv())){
                            k.setDaLiJeObojena(true);
                        }
                    }

                }
            });
        }else if(getArguments().containsKey("poslaniautor")){

            ovogautoratrazis=getArguments().getParcelable("poslaniautor");

            long id=KategorijeAkt.boh.idAutora(ovogautoratrazis.getImeiPrezime());

            // ArrayList<Knjiga>odabraneknjige=new ArrayList<Knjiga>();
            /*for(Knjiga x: KategorijeAkt.knjige){
                //trebas napraviti da prolazi kroz listu autora i da vidi sdrzi li ovog
                if(x.getAutori().contains(ovogautoratrazis.getImeiPrezime())){
                    odabraneknjige.add(0,x);
                }
            }*/
            //long id=Long.getLong(idautora);
            odabraneknjige=KategorijeAkt.boh.knjigeAutora(id);
            ListView lista = (ListView) getView().findViewById(R.id.listaKnjiga);
            AdapterZaKnjige adaptercic = new AdapterZaKnjige(getActivity(), odabraneknjige, new AdapterZaKnjige.OnChangeFragmentListener() {
                @Override
                public void changeFragment(int position) {
                    Bundle argumenti = new Bundle();
                    argumenti.putParcelable("knjiga", KnjigeFragment.odabraneknjige.get(position));
                    android.app.FragmentManager fm = getFragmentManager();
                    FragmentPreporuci fp;
                    fp = new FragmentPreporuci();
                    fp.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.fFragmenti, fp).addToBackStack(null).commit();
                }
            });
            lista.setAdapter(adaptercic);

            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    view.setBackgroundColor(getActivity().getResources().getColor(R.color.bojaZaElementLise));
                    odabraneknjige.get(position).setDaLiJeObojena(true);
                    for (Knjiga k: KategorijeAkt.knjige) {
                        if(k.getNaziv().equals(odabraneknjige.get(position).getNaziv())){
                            k.setDaLiJeObojena(true);
                        }
                    }

                }
            });

        }

        ListView listica= (ListView) getView().findViewById(R.id.listaKnjiga);


        //MORAS NAPRAVITI DA SE VRACA NA PROSLI FRAGMENT !!!!!
        /*
        Button povratak=(Button) findViewById(R.id.dPovratak);
        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });*/
    }


}

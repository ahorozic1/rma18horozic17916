package com.example.user.spirala;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class KnjigePoznanika extends IntentService {
    final public static int STATUS_START = 0;
    final public static int STATUS_FINISH = 1;
    final public static int STATUS_ERROR = 2;
    ArrayList<Knjiga> rezi = new ArrayList<>();
    final String API_KEY="AIzaSyDCtiDwiXxS4lHWafsI_KuyFRnxV8SWDNk";

    //konstruktor
    public KnjigePoznanika(){super(null);}
    public KnjigePoznanika(String ime) {
        super(ime);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        //sta servis radi ide tu
        final ResultReceiver r = intent.getParcelableExtra("r");
        Bundle b = new Bundle();
        r.send(STATUS_START, Bundle.EMPTY);

        String imekorisnika=intent.getStringExtra("idKorisnika");

        String query = null;

        try {
            query = URLEncoder.encode(imekorisnika, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            r.send(STATUS_ERROR, b.EMPTY);
            //fali ti kljuc
        }
        String url1 = "https://www.googleapis.com/books/v1/users/" + query + "/bookshelves?key=" + API_KEY;

        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rez = convertStreamToString(in);
            JSONObject jo = new JSONObject(rez);
            JSONArray police = jo.getJSONArray("items");
            //id, naziv,autori, opis, datumobjave, slika,  brojstranica -Atributi za knjigu koji mi trebaju
            JSONArray napolici = jo.getJSONArray("items");
            for (int k = 0; k < napolici.length(); k++) {
                JSONObject p = napolici.getJSONObject(k);
                String idpolice = p.getString("id");
                String  urlzapolicu="https://www.googleapis.com/books/v1/users/" + query + "/bookshelves/" + idpolice +"/volumes?key=" + API_KEY;
                try {
                    URL urlzapolicu1 = new URL(urlzapolicu);
                    HttpURLConnection urlConnectionpolica = (HttpURLConnection) urlzapolicu1.openConnection();
                    InputStream inpolice = new BufferedInputStream(urlConnectionpolica.getInputStream());
                    String knji = convertStreamToString(inpolice);
                    JSONObject books = new JSONObject(knji);
                    try {
                        JSONArray knjige = books.getJSONArray("items");
                        for (int i = 0; i < knjige.length(); i++) {
                            JSONObject knjiga = knjige.getJSONObject(i);
                            String id = knjiga.getString("id");
                            //t id=Integer.parseInt(idS);
                            JSONObject informacije = knjiga.getJSONObject("volumeInfo");
                            String naziv = informacije.getString("title");

                            //atributi koji bi mogli biti null
                            JSONArray autori = null;
                            ArrayList<Autor> autorilista = null;
                            try {
                                autori = informacije.getJSONArray("authors");
                                autorilista = new ArrayList<Autor>();

                                if (autori != null) {
                                    for (int j = 0; j < autori.length(); j++) {
                                        autorilista.add(new Autor(autori.getString(j), id));
                                    }
                                }
                            } catch (JSONException e) {
                            }
                            String opis = null;
                            try {
                                opis = informacije.getString("description");
                            } catch (JSONException e) {
                            }

                            String datumobjave = null;

                            try {
                                datumobjave = informacije.getString("publishedDate");
                            } catch (JSONException e) {
                            }

                            JSONObject slike = null;
                            String slikaS = null;
                            URL slika = null;

                            try {
                                slike = informacije.getJSONObject("imageLinks");
                                slikaS = slike.getString("thumbnail");
                                slika = new URL(slikaS);
                            } catch (JSONException e) {
                            }

                            String brojstranicaS = null;
                            int brojstranica = 0;
                            try {
                                brojstranicaS = informacije.getString("pageCount");
                                brojstranica = Integer.parseInt(brojstranicaS);
                            } catch (JSONException e) {
                            }

                            rezi.add(new Knjiga(id, naziv, autorilista, datumobjave, opis, slika, brojstranica));
                        }
                    } catch (JSONException e) {
                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    r.send(STATUS_ERROR,Bundle.EMPTY);
                } catch (IOException e) {
                    e.printStackTrace();
                    r.send(STATUS_ERROR,Bundle.EMPTY);
                } catch (JSONException e) {
                    e.printStackTrace();
                    r.send(STATUS_ERROR,Bundle.EMPTY);
                }
            }
        }   catch (MalformedURLException e) {
            e.printStackTrace();
            r.send(STATUS_ERROR, Bundle.EMPTY);
        } catch (IOException e) {
            e.printStackTrace();
            r.send(STATUS_ERROR, Bundle.EMPTY);
        } catch (JSONException e) {
            e.printStackTrace();
            r.send(STATUS_ERROR, Bundle.EMPTY);
        }



        b.putParcelableArrayList("result",  rezi);
        r.send(STATUS_FINISH, b);

    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {

        } finally {

            try {
                is.close();
            } catch (IOException e) {

            }
        }
        return sb.toString();


    }
}

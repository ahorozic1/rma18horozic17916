package com.example.user.spirala;

import android.os.AsyncTask;

import com.example.user.spirala.Autor;
import com.example.user.spirala.Knjiga;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class DohvatiKnjige extends AsyncTask<String, Integer, Void> {

    public interface IDohvatiKnjigeDone {
        public void onDohvatiDone(ArrayList<Knjiga> rezultat);

    }
    public ArrayList<Knjiga> rezultat= new ArrayList<Knjiga>();
    private IDohvatiKnjigeDone pozivatelj;

    public DohvatiKnjige(IDohvatiKnjigeDone p) {
        pozivatelj=p;
    }


    @Override
    protected Void doInBackground(String... strings) {
        for( String s: strings) {
            String query = null;
            try {
                query = URLEncoder.encode(strings[0], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String url1 = "https://www.googleapis.com/books/v1/volumes?q=intitle:" + query ;
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                //urlConnection.setRequestProperty("Authorization", "Bearer"+ );
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String rez = convertStreamToString(in);
                JSONObject jo = new JSONObject(rez);

                //JSONObject knjige=jo.getJSONObject(("Items"));

                JSONArray knjige = jo.getJSONArray("items");

                for (int i = 0; i < 5; i++) {
                    JSONObject knjiga = knjige.getJSONObject(i);
                    //id, naziv,autori, opis, datumobjave, slika,  brojstranica
                    String id = knjiga.getString("id");
                    JSONObject informacije = knjiga.getJSONObject("volumeInfo");

                    //ne mora savaka knjigati ima ove at
                    String naziv = null;
                    String imeAutora="";
                    JSONArray autori = null;
                    ArrayList<Autor> autorilista = null;
                    String datum = null;
                    JSONObject slika = null;
                    String linkslike = null;
                    URL urlslika = null;
                    int intbrojstranica =0;
                    String brojstranica = null;
                    String opis=null;
                    autorilista = new ArrayList<Autor>();


                    try {
                        naziv = informacije.getString("title");
                    } catch (JSONException e) {
                    }
                    try {
                        autori = informacije.getJSONArray("authors");

                        //TREBAS SAD IZ JSONA U NIZ AUTORA PREBACIT NEKAKO
                        if (autori.length() != 0) {
                            for (int j = 0; j < autori.length(); j++) {
                                autorilista.add(new Autor(autori.getString(j), id));
                                String autorisvi=autori.getString(j)+" ";
                                imeAutora.concat(autorisvi);
                            }
                        }
                    } catch (JSONException e) {
                    }

                    try {
                        datum = informacije.getString("publishedDate");
                    } catch (JSONException e) {
                    }

                    try {
                        slika = informacije.getJSONObject("imageLinks");
                        if(slika!=null) {
                            linkslike = slika.getString("thumbnail");
                            urlslika = new URL(linkslike);
                        }
                    } catch (JSONException e) {
                    }
                    try {
                        brojstranica = informacije.getString("pageCount");
                        intbrojstranica = Integer.parseInt(brojstranica);
                    } catch (JSONException e) {
                    }

                    try {

                        opis = informacije.getString("description");
                    } catch (JSONException e) {
                    }
                    Knjiga nova=new Knjiga(id, naziv, autorilista, opis, datum, urlslika, intbrojstranica);
                    nova.setImeAutora(imeAutora);
                    rezultat.add(nova);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    //TREBAS IMPLEMENTIRATI METODU ONDONE U FRAGMENTU !!!
    //@Override ​​
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                pozivatelj.onDohvatiDone(rezultat);
            }

            public String convertStreamToString(InputStream is){
                BufferedReader reader=new BufferedReader(new InputStreamReader(is));
                StringBuilder sb=new StringBuilder();
                String line=null;
                try{
                    while((line=reader.readLine())!=null){
                        sb.append(line+"\n");
                    }
                }catch(IOException e){

                }finally {

                        try{
                            is.close();
                        }catch(IOException e){

                        }
                    }
                    return sb.toString();
    }
}



package com.example.user.spirala;

import android.app.Fragment;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class FragmentOnline extends Fragment implements DohvatiKnjige.IDohvatiKnjigeDone, DohvatiNajnovije.IDohvatiNajnovijeDone,MojResultReceiver.Receiver {

    ArrayList<Knjiga> vraceneknjige= new ArrayList<>();
    boolean biloihjevise = false;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_online, container, false);

    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        final Spinner spinnerzarezultate=(Spinner)getView().findViewById(R.id.sRezultat);
        final Spinner spinerzakategorije=(Spinner)getView().findViewById((R.id.sKategorije));
        Button dugmerun=(Button) getView().findViewById(R.id.dRun);
        final Button dugmeadd=(Button) getView().findViewById(R.id.dAdd);
        dugmeadd.setEnabled(false);
        Button dugmepovratak=(Button)getView().findViewById(R.id.dPovratak);
        final EditText upisanitekst=(EditText) getView().findViewById(R.id.tekstUpit);

        //dodavanje u spiner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, KategorijeAkt.kategorije );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerzakategorije.setAdapter(adapter);


         dugmepovratak.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 //trebam li sad provjeravati da li je otvoren neki fragemnt
                 EditText tekst=(EditText)getView().findViewById(R.id.tekstUpit);
                 Spinner spiner=(Spinner)getView().findViewById(R.id.sRezultat);
                 tekst.setText("");
                    Button dugmeautori=(Button)getActivity().findViewById(R.id.dAutori);
                    dugmeautori.setVisibility( View.VISIBLE);
                    Button dugmekategorije=(Button)getActivity().findViewById(R.id.dKategorije);
                    dugmekategorije.setVisibility(View.VISIBLE);
                    Button online=(Button)getActivity().findViewById(R.id.dDodajOnline);
                    online.setVisibility(View.VISIBLE);
                    getFragmentManager().popBackStack();
             }
         });
        dugmeadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               int pozicija=spinnerzarezultate.getSelectedItemPosition();
               Knjiga odabrana=vraceneknjige.get(pozicija);

               ArrayList<Autor> autoriodabrane=odabrana.getAutori();
               //postavljas joj ostale atribute koji nisu mogli bitit postavljeni na osnovu jsona
               odabrana.setDaLiJeObojena(false);
               odabrana.setKategorija(spinerzakategorije.getSelectedItem().toString());
               //provjera da li postoji neki od autora odabrane knjige u listi autora
                if(autoriodabrane!=null) {
                    for (int j = 0; j < autoriodabrane.size(); j++) {
                        boolean ima = false;
                        for (int i = 0; i < KategorijeAkt.autori.size(); i++) {
                            if (KategorijeAkt.autori.get(i).getImeiPrezime().equals(autoriodabrane.get(j).getImeiPrezime())) {
                                ima = true;
                                KategorijeAkt.autori.get(i).setBrojKnjiga(KategorijeAkt.autori.get(i).getBrojKnjiga() + 1);
                                KategorijeAkt.autori.get(i).dodajKnjigu(odabrana.getId());
                            }
                        }
                        if (!ima) {
                            Autor novi = new Autor(autoriodabrane.get(j).getImeiPrezime(), odabrana.getId());
                            novi.setBrojKnjiga(1);
                            KategorijeAkt.autori.add(novi);
                        }
                    }
                }

                KategorijeAkt.knjige.add(0, odabrana);
                KategorijeAkt.boh.dodajKnjigu(odabrana);




        }});

        dugmerun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 biloihjevise =false;

                //zavisno od teksta koji webservis
                String tekst=upisanitekst.getText().toString();
                //razdvojis sting na rijeci


                 if(tekst.contains(";")){//ako ima vise knjiga odvojenih sa ;
                    biloihjevise =true;
                    Toast.makeText(getActivity(), "Vise je rijeci!", Toast.LENGTH_LONG).show();
                    String [] nizrijeci=tekst.trim().split(";");
                    for(String x: nizrijeci){
                        new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone)FragmentOnline.this).execute(x);
                    }
                } else if(tekst.length()>6 && tekst.substring(0,6).equals("autor:")){ //ako ima rijec autor da se filtrira po autorima
                    String autor=tekst.substring(6,tekst.length());
                     Toast.makeText(getActivity(), "Po autoru   !", Toast.LENGTH_LONG).show();
                    new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone)FragmentOnline.this).execute(autor);
                }else if(tekst.length()>9 && tekst.substring(0,9).equals("korisnik:")){
                    String korisnik=tekst.substring(9,tekst.length());
                     Toast.makeText(getActivity(), "Po korisniku!", Toast.LENGTH_LONG).show();
                    MojResultReceiver mReceiver = new MojResultReceiver(new Handler());
                    mReceiver.setReceiver(FragmentOnline.this);
                    Intent i=new Intent(Intent.ACTION_SYNC, null, getActivity(), KnjigePoznanika.class);
                    i.putExtra("idKorisnika", korisnik);
                    i.putExtra("r", mReceiver);
                    getActivity().startService(i);
                }else if(!tekst.contains(";")) {
                     String[] nizunesenihrijeci = tekst.trim().split(" ");
                     if (nizunesenihrijeci.length == 1) { //ako ima samo jedna knjiga
                         //tu pozivas web serivs
                         Toast.makeText(getActivity(), "Jedna je rijec!", Toast.LENGTH_LONG).show();
                         new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone) FragmentOnline.this).execute(tekst);
                     }
                 }
                 dugmeadd.setEnabled(true);

            }
        });
    }

    @Override
    public void onDohvatiDone(ArrayList<Knjiga> rezultat) {
        if(biloihjevise) {
            for (int i = 0; i < rezultat.size(); i++) {
                vraceneknjige.add(rezultat.get(i));
            }
        }
        else {
            vraceneknjige.clear();
            for (int i = 0; i < rezultat.size(); i++) {
                vraceneknjige.add(rezultat.get(i));
            }
        }
        //spiner punis rezultatima
        Spinner spinerrezultata=(Spinner)  getView().findViewById(R.id.sRezultat);
        if(vraceneknjige.size()!=0) {
            AdapterZaSpinerKnjiga ad=new AdapterZaSpinerKnjiga(getActivity(),vraceneknjige);
            ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinerrezultata.setAdapter(ad);

        }


    }

    @Override
    public void onNajnovijeDone(ArrayList<Knjiga> rezultat) {
        vraceneknjige.clear();
        for (int i = 0; i < rezultat.size(); i++) {
            vraceneknjige.add(rezultat.get(i));
        }
        Spinner spinerrezultata=(Spinner)  getView().findViewById(R.id.sRezultat);
        if(vraceneknjige.size()!=0) {
            AdapterZaSpinerKnjiga ad=new AdapterZaSpinerKnjiga(getActivity(),vraceneknjige);
            ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinerrezultata.setAdapter(ad);

        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

       switch(resultCode){
            case KnjigePoznanika.STATUS_START:
                    break;
            case KnjigePoznanika.STATUS_FINISH: //treba staviti u spiner rezultate
                Spinner spinerrez=(Spinner)getView().findViewById(R.id.sRezultat);
                vraceneknjige=resultData.getParcelableArrayList("result");
                    AdapterZaSpinerKnjiga ad=new AdapterZaSpinerKnjiga(getActivity(),vraceneknjige);
                    ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinerrez.setAdapter(ad);


                break;
            case KnjigePoznanika.STATUS_ERROR:
                String error=resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                break;
        }
    }



}

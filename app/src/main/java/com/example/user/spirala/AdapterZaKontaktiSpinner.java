package com.example.user.spirala;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterZaKontaktiSpinner extends ArrayAdapter<Kontakt> {

    private final LayoutInflater inflater;
    private final Context mContext;



    public AdapterZaKontaktiSpinner(@NonNull Context context, ArrayList<Kontakt> k) {
        super(context, 0,k);
        inflater=LayoutInflater.from(context);
        mContext=context;

    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

       /* if (convertView== null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.element_spinera_kontakti, parent, false);
        }*/

        /*
        TextView tekst=(TextView) convertView.findViewById(R.id.tImeiMail);
        tekst.setText(getItem(position).getIme()+": "+getItem(position).getEmail());*/

        //return super.getView(position, convertView, parent);
        return getCustomView(position,convertView, parent);

    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {


        View row=inflater.inflate(R.layout.element_spinera_kontakti, parent, false);
        TextView tekst=(TextView)convertView.findViewById(R.id.tImeiMail);
        tekst.setText(getItem(position).getIme()+": "+getItem(position).getEmail());

        return row;


    }
}


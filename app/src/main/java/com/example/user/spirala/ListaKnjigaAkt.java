package com.example.user.spirala;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ListaKnjigaAkt extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_knjiga_akt);
        Intent intent = getIntent();
        String ovukategorijutrazis = intent.getStringExtra("imeKategorije");
        ArrayList<Knjiga> odabraneknjige= new ArrayList<Knjiga>();

        for(int i=0; i<KategorijeAkt.knjige.size(); i++){
            if(KategorijeAkt.knjige.get(i).getKategorija().equals(ovukategorijutrazis))  odabraneknjige.add(KategorijeAkt.knjige.get(i));
        }
        //odabraneknjige.add(0, new Knjiga("Asja i patka", "Patka", "Asja Horozic", "Asja"  ));
        //odabraneknjige.add(0, new Knjiga("Koaline pustilovine", "Patka", "Koala Koalic", "Sport1"));
        //odabraneknjige.add(0, new Knjiga("Mali doktor", "Patka", "Kike Budalike", "Asja"  ));
        //odabraneknjige.add(0, new Knjiga("Paja patak", "Patka", "PEro peric", "Sport1"));

        ListView lista=(ListView)findViewById(R.id.listaKnjiga);
        AdapterZaKnjige adaptercic= new AdapterZaKnjige(this, odabraneknjige);
        lista.setAdapter(adaptercic);

        Button povratak=(Button) findViewById(R.id.dPovratak);
        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

       //rayAdapter<String> adapterzalistu = new ArrayAdapter<String>(this,R.layout.element_liste,R.layout.odabraneknjige);

    }
}

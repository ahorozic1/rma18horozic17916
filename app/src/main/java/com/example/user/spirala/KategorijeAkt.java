package com.example.user.spirala;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;

public class KategorijeAkt extends AppCompatActivity {

    private ArrayAdapter<String> myAdapterInstance;
    public static ArrayList<String>  kategorije;
    public static ArrayList<Knjiga> knjige;
    public static ArrayList<Autor> autori;
    public static BazaOpenHelper boh;
    public  static ArrayList<Kontakt> kontakti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //getBaseContext().deleteDatabase(boh.DATABASENAME);
        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this);
        setContentView(R.layout.activity_kategorije_akt);
        boh= new BazaOpenHelper(this.getBaseContext());


        knjige=new ArrayList<Knjiga>();
        knjige=boh.izvadiKnjige();
        kategorije=new ArrayList<String>();
        kategorije=boh.izvadiKategorije();
        autori=new ArrayList<Autor>();
        autori=boh.izvadiAutore();

        kontakti=new ArrayList<Kontakt>();
        /*autori.add(new Autor("Asja HOrozic"));
        autori.add(new Autor("dzanko"));*/


         Button dugmeautori= (Button) findViewById(R.id.dAutori);
         Button dugmekategorije= (Button) findViewById(R.id.dKategorije);
         Button dugmedodajonline=(Button) findViewById(R.id.dDodajOnline);

         dugmedodajonline.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 android.app.FragmentManager fm= getFragmentManager();
                 FragmentOnline fo;
                 fo=new FragmentOnline();
                 Bundle argumenti=new Bundle();
                 argumenti.putStringArrayList("kategorije", kategorije);
                 fo.setArguments(argumenti);
                 fm.beginTransaction().replace(R.id.fFragmenti, fo).addToBackStack(null).commit();
             }
         });



         dugmekategorije.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 android.app.FragmentManager fm = getFragmentManager();
                 ListeFragment lf;
                //kreiramo novi fragment FragmentDetalji ukoliko već nije kreiran
                     lf=new ListeFragment();
                     Bundle argumenti = new Bundle();
                     argumenti.putStringArrayList("kategorije", kategorije);
                     lf.setArguments(argumenti);
                     fm.beginTransaction().replace(R.id.fFragmenti, lf).addToBackStack(null).commit();
                 }

         });

         dugmeautori.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 android.app.FragmentManager fm = getFragmentManager();
                 ListeFragment lf;
                 //provjerimo da li je fragment već kreiran

                     //kreiramo novi fragment FragmentDetalji ukoliko već nije kreiran
                     lf=new ListeFragment();
                     Bundle argumenti = new Bundle();
                     argumenti.putParcelableArrayList("autori", autori);
                     lf.setArguments(argumenti);
                     fm.beginTransaction().replace(R.id.fFragmenti, lf).addToBackStack(null).commit();
                 }


             });






    }
}

package com.example.user.spirala;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.ArrayAdapter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class BazaOpenHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION=1;
    public static final String DATABASENAME="spirala4Baza.db";
    public static  final String DATABASE_TABLE_KATEGORIJA="Kategorija";
    public static final String DATABASE_TABLE_KNJIGA="Knjiga";
    public static final String DATABASE_TABLE_AUTOR="Autor";
    public static final String DATABASE_TABLE_AUTORSTVO="Autorstvo";
    public static final String KATEGORIJA_ID="_id";
    public static final String KATEGORIJA_NAZIV="naziv";
    public static final String KNJIGA_ID="_id";
    public static final String KNJIGA_NAZIV="naziv";
    public static final String KNJIGA_OPIS="opis";
    public static final String KNJIGA_DATUM="datumObjavljivanja";
    public static final String KNJIGA_STRANICE="brojStranica";
    public static final String KNJIGA_WEB="idWebServis";
    public static final String KNJIGA_KATEGORIJA="idKategorije";
    public static final String KNJIGA_SLIKA="slika";
    public static final String KNJIGA_PREGLEDANA="pregledana";
    public static final String AUTOR_ID="_id";
    public static final String AUTOR_IME="ime";
    public static final String AUTORSTVO_ID="_id";
    public static final String AUTORSTVO_AUTOR="idautora";
    public static final String AUTORSTVO_KNJIGA="idknjige";


    private static final String DATABASE_CREATE_KATEGORIJA= "CREATE TABLE " + DATABASE_TABLE_KATEGORIJA + "("+ KATEGORIJA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+ KATEGORIJA_NAZIV+" text);";
    private static final String DATABASE_CREATE_KNJIGA= "create table " + DATABASE_TABLE_KNJIGA + "("+ KNJIGA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+ KNJIGA_NAZIV+" text not null, "+ KNJIGA_OPIS+ " text, "+ KNJIGA_DATUM+ " text, "+ KNJIGA_STRANICE+ " integer, "+ KNJIGA_WEB+ " text, "+ KNJIGA_KATEGORIJA+ " text not null, "+ KNJIGA_SLIKA+" text, "+ KNJIGA_PREGLEDANA+" integer);";
    private static final String DATABASE_CREATE_AUTOR= "create table " + DATABASE_TABLE_AUTOR + "("+ AUTOR_ID + " integer primary key autoincrement, "+ AUTOR_IME+" text);";
    private static final String DATABASE_CREATE_AUTORSTVO= "create table " + DATABASE_TABLE_AUTORSTVO + "("+ AUTORSTVO_ID + " integer primary key autoincrement, "+ AUTORSTVO_AUTOR+" integer, "+ AUTORSTVO_KNJIGA+" integer);";

    String[] kolonerezultatautor=new String[]{BazaOpenHelper.AUTOR_ID, BazaOpenHelper.AUTOR_IME};
    String [] kolonezakategorija= new String[] {BazaOpenHelper.KATEGORIJA_ID, BazaOpenHelper.KATEGORIJA_NAZIV};
    String[] kolonezaknjiga=new String[] {BazaOpenHelper.KNJIGA_ID, BazaOpenHelper.KNJIGA_NAZIV, BazaOpenHelper.KNJIGA_OPIS, BazaOpenHelper.KNJIGA_DATUM, BazaOpenHelper.KNJIGA_STRANICE, BazaOpenHelper.KNJIGA_WEB, BazaOpenHelper.KNJIGA_KATEGORIJA, BazaOpenHelper.KNJIGA_SLIKA, BazaOpenHelper.KNJIGA_PREGLEDANA};
    String[] kolonezaautorstvo=new String[]{BazaOpenHelper.AUTORSTVO_ID, BazaOpenHelper.AUTORSTVO_AUTOR, BazaOpenHelper.AUTORSTVO_KNJIGA};

    public BazaOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public BazaOpenHelper(Context context) {
        super(context, DATABASENAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DATABASE_CREATE_KNJIGA);
        sqLiteDatabase.execSQL(DATABASE_CREATE_KATEGORIJA);
        sqLiteDatabase.execSQL(DATABASE_CREATE_AUTOR);
        sqLiteDatabase.execSQL(DATABASE_CREATE_AUTORSTVO);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ DATABASE_TABLE_KNJIGA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ DATABASE_TABLE_AUTOR);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ DATABASE_TABLE_KATEGORIJA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ DATABASE_TABLE_AUTORSTVO);
        onCreate(sqLiteDatabase);
    }

    long dodajKnjigu(Knjiga knjiga) {
        SQLiteDatabase db;
        long vratime = -1;
        try {
            db = this.getWritableDatabase();
        } catch (Exception e) {
            return vratime;
        }

        ContentValues noviautor = new ContentValues();
        ContentValues novoautorstvo = new ContentValues();
        ContentValues novaknjiga = new ContentValues();

        String idkategorije = null;
        String idautora = null;
        String idknjigenove = null;
        Cursor cursor = null;
        String where = null;
        String idknjige = knjiga.getId();


        //DODAVANJE AUTORA, KNJIGE I AUTORSTVO
        if (!knjiga.getDodanaOnline()) { //ako je dodana normalno

            //DODAVANJE AUTOR
            where = BazaOpenHelper.AUTOR_IME + "=" + "'"+ knjiga.getImeAutora()+"'";
            cursor = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, kolonerezultatautor, where, null, null, null, null);

            if (cursor.getCount() == 0) {
                noviautor.put(BazaOpenHelper.AUTOR_IME, knjiga.getImeAutora());
                db.insert(BazaOpenHelper.DATABASE_TABLE_AUTOR, null, noviautor);
            }
            //DODAVANJE U KNJIGE

            //kupis id kategorije iz tabele kategorija
            where = BazaOpenHelper.KATEGORIJA_NAZIV + "=" + "'"+ knjiga.getKategorija()+"'";
            cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KATEGORIJA, kolonezakategorija, where, null, null, null, null);
            if(cursor.moveToNext() && cursor.getCount()>0) {
                idkategorije = cursor.getString(cursor.getColumnIndexOrThrow(BazaOpenHelper.KATEGORIJA_ID));
            }
            //dodajes novu knjigu
            novaknjiga.put(BazaOpenHelper.KNJIGA_NAZIV, knjiga.getNaziv());
            novaknjiga.put(BazaOpenHelper.KNJIGA_KATEGORIJA, idkategorije);
            Integer obojena= knjiga.isDaLiJeObojena()? 1:0;
            novaknjiga.put(BazaOpenHelper.KNJIGA_PREGLEDANA, obojena);
           vratime= db.insert(BazaOpenHelper.DATABASE_TABLE_KNJIGA, null, novaknjiga);

            //DODAVANJE U AUTORSTVO
            //kupis id knjige iz tabele knjiga
            where = BazaOpenHelper.KNJIGA_NAZIV + "=" + "'"+knjiga.getNaziv()+"'";
            cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KNJIGA, kolonezaknjiga, where, null, null, null, null);
            if(cursor.moveToNext() && cursor.getCount()>0) {
                idknjigenove = cursor.getString(cursor.getColumnIndexOrThrow(BazaOpenHelper.KNJIGA_ID));
            }
            //kupis id autora iz tabele autor
            where = BazaOpenHelper.AUTOR_IME + "=" + "'"+ knjiga.getImeAutora()+"'";
            cursor = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, kolonerezultatautor, where, null, null, null, null);
            if(cursor.moveToNext() && cursor.getCount()>0) {
                idautora = cursor.getString(cursor.getColumnIndexOrThrow(BazaOpenHelper.AUTOR_ID));
            }
            //dodajes novo autorstvo
            novoautorstvo.put(BazaOpenHelper.AUTORSTVO_KNJIGA, idknjigenove);
            novoautorstvo.put(BazaOpenHelper.AUTORSTVO_AUTOR, idautora);
             db.insert(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, null, novoautorstvo);



        } else if (knjiga.getDodanaOnline()) {//ako je dodan online
            //DODAVANJE U AUTORE
            ArrayList<Autor> autoriknjige=new ArrayList<Autor>();
            if (knjiga.getAutori() != null) {
                autoriknjige = knjiga.getAutori();
                for (Autor x : autoriknjige) {
                    String ime = x.getImeiPrezime();
                    where = BazaOpenHelper.AUTOR_IME + "=" + "'" + ime + "'";
                    cursor = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, kolonerezultatautor, where, null, null, null, null);
                    if (cursor.getCount() == 0 ) {
                        noviautor.put(BazaOpenHelper.AUTOR_IME, x.getImeiPrezime());
                        vratime=db.insert(BazaOpenHelper.DATABASE_TABLE_AUTOR, null, noviautor);
                    }
                }
            }
            //DODAVANJE U KNJIGE
            //kupis id kategorije iz tabele kategorije
            where=BazaOpenHelper.KNJIGA_NAZIV + "=" + "'" + knjiga.getNaziv() + "'";
            //where = BazaOpenHelper.KNJIGA_NAZIV + "=" +"'"+ knjiga.getNaziv()+"'";
            cursor=db.query(BazaOpenHelper.DATABASE_TABLE_KNJIGA, kolonezaknjiga, where, null,null,null,null );
            if(cursor.getCount()==0){

                where = BazaOpenHelper.KATEGORIJA_NAZIV + "=" +"'"+knjiga.getKategorija()+"'";
                cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KATEGORIJA, kolonezakategorija, where, null, null, null, null);
                if (cursor.getCount() >0 && cursor.moveToNext()) {
                    idkategorije = cursor.getString(cursor.getColumnIndexOrThrow(BazaOpenHelper.KATEGORIJA_ID));
                }

                // dodavanje u tabelu knjiga
                novaknjiga.put(BazaOpenHelper.KNJIGA_KATEGORIJA, idkategorije);
                novaknjiga.put(BazaOpenHelper.KNJIGA_NAZIV, knjiga.getNaziv());
                novaknjiga.put(BazaOpenHelper.KNJIGA_OPIS, knjiga.getOpis());
                novaknjiga.put(BazaOpenHelper.KNJIGA_DATUM, knjiga.getDatumObjavljivanja());
                novaknjiga.put(BazaOpenHelper.KNJIGA_STRANICE, knjiga.getBrojStranica());
                novaknjiga.put(BazaOpenHelper.KNJIGA_WEB, knjiga.getId());

                try {
                    String slika=knjiga.getSlika().toString();
                    novaknjiga.put(BazaOpenHelper.KNJIGA_SLIKA, slika);
                } catch(Exception e){}

                Integer obojena= knjiga.isDaLiJeObojena()? 1:0;
                novaknjiga.put(BazaOpenHelper.KNJIGA_PREGLEDANA, obojena);
                vratime=db.insert(BazaOpenHelper.DATABASE_TABLE_KNJIGA, null, novaknjiga);

            }

            //DODAVANJE U AUTORSTVO
                //kupis id knjige iz tabele knjiga
                where = BazaOpenHelper.KNJIGA_NAZIV + "=" +"'"+ knjiga.getNaziv()+"'";
                cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KNJIGA, kolonezaknjiga, where, null, null, null, null);
                if(cursor.getCount()==1 && cursor.moveToNext() ) {
                    idknjigenove = cursor.getString(cursor.getColumnIndexOrThrow(BazaOpenHelper.KNJIGA_ID));
                }

                for (Autor x : autoriknjige) {
                    //kupis id autora iz tabele autor
                    where = BazaOpenHelper.AUTOR_IME + "=" + "'"+x.getImeiPrezime()+"'";
                    cursor = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, kolonerezultatautor, where, null, null, null, null);
                    if(cursor.getCount()==1 && cursor.moveToNext()){
                        idautora = cursor.getString(cursor.getColumnIndexOrThrow(BazaOpenHelper.AUTOR_ID));
                    }

                    //dodajes u tabelu autorstvo
                    novoautorstvo.put(BazaOpenHelper.AUTORSTVO_KNJIGA, idknjigenove);
                    novoautorstvo.put(BazaOpenHelper.AUTORSTVO_AUTOR, idautora);
                    db.insert(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, null, novoautorstvo);


                }

                }
                cursor.close();
        db.close();
        return vratime;


    }


    public long dodajKategoriju(String naziv){

        long vratime=-1;
        SQLiteDatabase db;

        try{

           db= getWritableDatabase();
        }catch(Exception e){
            return -1;
        }

        ContentValues novi=new ContentValues();
        novi.put(KATEGORIJA_NAZIV, naziv);

        String where = BazaOpenHelper.KATEGORIJA_NAZIV + "=" + "'" + naziv + "'";
        Cursor cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KATEGORIJA, kolonezakategorija, where, null, null, null, null);
        if (cursor.getCount() == 0 ) {

            vratime= db.insert(DATABASE_TABLE_KATEGORIJA, null, novi);

        }
        cursor.close();
        db.close();
       return vratime;

    }

    public ArrayList<String> izvadiKategorije(){
        SQLiteDatabase db;
        ArrayList<String> kate=new ArrayList<String>();
       db=this.getReadableDatabase();
       Cursor kursor=db.query(BazaOpenHelper.DATABASE_TABLE_KATEGORIJA, kolonezakategorija, null,null,null,null,null,null);
       int pozicija=kursor.getColumnIndex(BazaOpenHelper.KATEGORIJA_NAZIV);
       while(kursor.moveToNext()){
           kate.add(kursor.getString(pozicija));
       }
       kursor.close();
       db.close();
       return kate;
    }

    public ArrayList<Knjiga> izvadiKnjige(){
        ArrayList<Knjiga> knjigice=new ArrayList<Knjiga>();
        SQLiteDatabase db;
        db=this.getReadableDatabase();
        Cursor kursor=db.query(BazaOpenHelper.DATABASE_TABLE_KNJIGA, kolonezaknjiga,null,null,null,null,null );
        int idind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_ID);
        int nazivind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_NAZIV);
        int opisind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_OPIS);
        int idwebind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_WEB);
        int brstranicaind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_STRANICE);
        int datumind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_DATUM);
        int slikaind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_SLIKA);
        int pregledanaind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_PREGLEDANA);
        while(kursor.getCount()>0 && kursor.moveToNext()){

            String idnove=kursor.getString(idind);
            String nazivnove=kursor.getString(nazivind);
            String opisnove=kursor.getString(opisind);
            String brojstrenicanove=kursor.getString(brstranicaind);
            String idwebnove=kursor.getString(idwebind);
            String datumnove=kursor.getString(datumind);
           String slikanove=kursor.getString(slikaind);
            String pregledananove=kursor.getString(pregledanaind);
            ArrayList<Autor> autorinove=new ArrayList<Autor>();
            String where2=BazaOpenHelper.AUTORSTVO_KNJIGA+"="+"'"+idnove+"'";
            Cursor autorstvo=db.query(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, kolonezaautorstvo, where2, null,null,null,null);
            while(autorstvo.moveToNext() ){
                int pozicija=autorstvo.getColumnIndex(BazaOpenHelper.AUTORSTVO_AUTOR);
                String autorid=autorstvo.getString(pozicija);
                String where3=BazaOpenHelper.AUTOR_ID+"="+"'"+autorid+"'";
                Cursor imeautora=db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, kolonerezultatautor,where3, null,null,null,null );
                int pozicija2=imeautora.getColumnIndex(BazaOpenHelper.AUTOR_IME);
                while(imeautora.moveToNext()){
                    autorinove.add(new Autor(imeautora.getString(pozicija2),idnove));
                }
                imeautora.close();

            }
            autorstvo.close();
            int stranicenove=Integer.parseInt(brojstrenicanove);
            try{
                URL url=new URL(slikanove);
                knjigice.add(new Knjiga(idnove,nazivnove, autorinove, opisnove, datumnove, url, stranicenove));
            } catch (MalformedURLException e){}



        }

        kursor.close();
        db.close();

        return knjigice;
    }

    ArrayList<Autor> izvadiAutore(){
        ArrayList<Autor> autorcici=new ArrayList<Autor>();
        SQLiteDatabase db;
        db=this.getReadableDatabase();
        Cursor kursor=db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, kolonerezultatautor,null,null,null,null,null );
        int idind=kursor.getColumnIndex(BazaOpenHelper.AUTOR_ID);
        int imeind=kursor.getColumnIndex(BazaOpenHelper.AUTOR_IME);

        while(kursor.getCount()>0 && kursor.moveToNext()) {

            String idnove = kursor.getString(idind);
            String imenove = kursor.getString(imeind);
            autorcici.add(new Autor(imenove, idnove));
        }

        kursor.close();
        db.close();

        return autorcici;
        }


    ArrayList<Knjiga> knjigeKategorije(long idKategorije){
        ArrayList<Knjiga> knjigice=new ArrayList<Knjiga>();
        SQLiteDatabase db;
        db=this.getReadableDatabase();
        String id = String.valueOf(idKategorije);
        String where= BazaOpenHelper.KNJIGA_KATEGORIJA+"="+"'"+id+"'";
        Cursor kursor=db.query(BazaOpenHelper.DATABASE_TABLE_KNJIGA, kolonezaknjiga,where,null,null,null,null );
        int idind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_ID);
        int nazivind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_NAZIV);
        int opisind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_OPIS);
        int idwebind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_WEB);
        int brstranicaind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_STRANICE);
        int datumind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_DATUM);
        int slikaind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_SLIKA);
        int pregledanaind=kursor.getColumnIndex(BazaOpenHelper.KNJIGA_PREGLEDANA);
        while(kursor.moveToNext()){

            String idnove=kursor.getString(idind);
            String nazivnove=kursor.getString(nazivind);
            String opisnove=kursor.getString(opisind);
            String brojstrenicanove=kursor.getString(brstranicaind);
            String idwebnove=kursor.getString(idwebind);
            String datumnove=kursor.getString(datumind);
            String slikanove=kursor.getString(slikaind);
            String pregledananove=kursor.getString(pregledanaind);
            ArrayList<Autor> autorinove=new ArrayList<Autor>();
            String where2=BazaOpenHelper.AUTORSTVO_KNJIGA+"="+"'"+idnove+"'";
            Cursor autorstvo=db.query(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, kolonezaautorstvo, where2, null,null,null,null);
            while(autorstvo.moveToNext() ){
                int pozicija=autorstvo.getColumnIndex(BazaOpenHelper.AUTORSTVO_AUTOR);
                String autorid=autorstvo.getString(pozicija);
                String where3=BazaOpenHelper.AUTOR_ID+"="+"'"+autorid+"'";
                Cursor imeautora=db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, kolonerezultatautor,where3, null,null,null,null );
                int pozicija2=imeautora.getColumnIndex(BazaOpenHelper.AUTOR_IME);
                while(imeautora.moveToNext()){
                    autorinove.add(new Autor(imeautora.getString(pozicija2),idnove));
                }
                imeautora.close();

            }
            autorstvo.close();

            int stranicenove = Integer.parseInt(brojstrenicanove);

            URL url=null;
            try{
               url=new URL(slikanove);

            } catch (MalformedURLException e){}

            knjigice.add(new Knjiga(idnove,nazivnove, autorinove, opisnove, datumnove, url, stranicenove));

        }

        kursor.close();
        db.close();

        return knjigice;
    }

    String dajIdKategorije(String nazivkategorije){
        SQLiteDatabase db=getReadableDatabase();
        String id=new String();
        String where=BazaOpenHelper.KATEGORIJA_NAZIV+"="+"'"+nazivkategorije+"'";
        Cursor kursor=db.query(BazaOpenHelper.DATABASE_TABLE_KATEGORIJA, kolonezakategorija,where, null,null,null,null);
        if(kursor.moveToNext()&& kursor.getCount()>0){
            int pozicija=kursor.getColumnIndex(BazaOpenHelper.KATEGORIJA_ID);
            id=kursor.getString(pozicija);
        }
        return id;
    }

    public ArrayList<Knjiga> knjigeAutora(long idAutora){
        SQLiteDatabase db=this.getReadableDatabase();
        ArrayList<Knjiga> knjigice = new ArrayList<>();


        String where=BazaOpenHelper.AUTORSTVO_AUTOR + "='" + Long.toString(idAutora)+"'";
        Cursor kursor= db.query(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, kolonezaautorstvo, where, null, null, null, null);

        int pozicija = kursor.getColumnIndex(BazaOpenHelper.AUTORSTVO_KNJIGA);//kolona gdje su id knjige koju je nap

        while (kursor.moveToNext()) {
            int idKnjige= kursor.getInt(pozicija);

            String where2=BazaOpenHelper.KNJIGA_ID + "='" + Integer.toString(idKnjige)+"'";

            Cursor kursor2 = db.query(BazaOpenHelper.DATABASE_TABLE_KNJIGA, kolonezaknjiga, where2, null, null, null, null);//trazis knjigu sa tim id iz autorstva

            int slikaind = kursor2.getColumnIndex(KNJIGA_SLIKA);
            int pregind = kursor2.getColumnIndex(KNJIGA_PREGLEDANA);
            int datumind = kursor2.getColumnIndex(BazaOpenHelper.KNJIGA_DATUM);
            int straniceind = kursor2.getColumnIndex(BazaOpenHelper.KNJIGA_STRANICE);
            int nazivind = kursor2.getColumnIndex(BazaOpenHelper.KNJIGA_NAZIV);
            int opisind = kursor2.getColumnIndex(BazaOpenHelper.KNJIGA_OPIS);
            int Webind = kursor2.getColumnIndex(BazaOpenHelper.KNJIGA_WEB);


            ArrayList<Autor> autorinove = new ArrayList<>();
            if(kursor2.getCount()==1 && kursor2.moveToNext()) {

                String novaslika = kursor2.getString(slikaind);
                String novapregledana = kursor2.getString(pregind);
                String novastranica = kursor2.getString(straniceind);
                String novadatum = kursor2.getString(datumind);
                String novanaziv = kursor2.getString(nazivind);
                String novaopis = kursor2.getString(opisind);
                String novaWeb = kursor2.getString(Webind);



                String where3=BazaOpenHelper.AUTORSTVO_KNJIGA + "=" +"'"+ Integer.toString(idKnjige)+"'";
                Cursor kursor3 = db.query(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO,kolonezaautorstvo , where3 , null, null, null, null);

                int autor = kursor3.getColumnIndex(BazaOpenHelper.AUTORSTVO_AUTOR);
                while (kursor3.moveToNext()) {
                    int autorID =kursor3.getInt(autor);
                    String where4=BazaOpenHelper.AUTOR_ID + "="+ "'" + Integer.toString(autorID) + "'";
                    Cursor kursor4 = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, kolonerezultatautor, where4,null, null, null, null);
                    if(kursor4.getCount()==1 && kursor4.moveToNext()) {
                        int pozicijaimena = kursor4.getColumnIndex(BazaOpenHelper.AUTOR_IME);
                        String imeautora=kursor4.getString(pozicijaimena);
                        autorinove.add(new Autor(imeautora, idKnjige));
                    }
                    kursor4.close();
                }
                kursor3.close();
                URL url=null;
                try{
                   url=new URL(novaslika);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                String idknjigestring=String.valueOf(idKnjige);
                int stranice=Integer.parseInt(novastranica);
                knjigice.add(new Knjiga(idknjigestring,novanaziv, autorinove, novaopis, novadatum, url, stranice));

            }
            kursor2.close();
        }


        kursor.close();
        db.close();

        return knjigice;
    }

    public long idAutora(String imeAutora) {

       SQLiteDatabase   db=this.getReadableDatabase();

        String where=BazaOpenHelper.AUTOR_IME + "="+ "'" + imeAutora+"'";
        Cursor kursor = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, kolonerezultatautor, where, null, null, null, null);
        if(kursor.getCount()==1 && kursor.moveToNext()) {
            int pozicija = kursor.getColumnIndex(BazaOpenHelper.AUTOR_ID);
            int idAutora = kursor.getInt(pozicija);
            kursor.close();
             return idAutora;

        }
        kursor.close();
        return -1;
    }




}

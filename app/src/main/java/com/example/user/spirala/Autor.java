package com.example.user.spirala;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Autor implements Parcelable {
   String imeiPrezime;
   int brojKnjiga;
   private ArrayList<String> knjige;

   public Autor(){}
   public Autor(String imeiPrezime, String id){
       this.imeiPrezime=imeiPrezime;
       this.knjige=new ArrayList<String>();
       this.knjige.add(id);
   }

    public Autor(String imeprezime) {
        this.imeiPrezime = imeprezime;
        this.brojKnjiga=1;
    }
    public Autor(String imeprezime, int brojKnjiga){
       this.imeiPrezime=imeprezime;
       this.brojKnjiga=brojKnjiga;
    }




    protected Autor(Parcel in) {
        imeiPrezime = in.readString();
        brojKnjiga = in.readInt();
    }

    public static final Creator<Autor> CREATOR = new Creator<Autor>() {
        @Override
        public Autor createFromParcel(Parcel in) {
            return new Autor(in);
        }

        @Override
        public Autor[] newArray(int size) {
            return new Autor[size];
        }
    };



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(imeiPrezime);
        //parcel.writeString(prezime);
        parcel.writeInt(brojKnjiga);
    }


    //geteri i seteri
    public String getImeiPrezime() {
        return imeiPrezime;
    }

    public void setImeiPrezime(String imeiPrezime) {
        this.imeiPrezime = imeiPrezime;
    }

    public int getBrojKnjiga() {
        return brojKnjiga;
    }

    public void setBrojKnjiga(int brojKnjiga) {
        this.brojKnjiga = brojKnjiga;
    }

    public ArrayList<String> getKnjige() {
        return knjige;
    }

    public void setKnjige(ArrayList<String> knjige) {
        this.knjige = knjige;
    }

    //trazena metoda
    public void dodajKnjigu(String id){
        if(!this.knjige.contains(id)) this.knjige.add(id);
    }
}

package com.example.user.spirala;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterZaSpinerKnjiga extends ArrayAdapter<Knjiga>{


    public AdapterZaSpinerKnjiga( Context context, ArrayList<Knjiga> k) {
        super(context, 0, k);
    }


    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView== null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.element_liste_autor, parent, false);

        }
        TextView mjestozaimeknjige=(TextView)convertView.findViewById(R.id.tImeiPrezime);
        TextView mjestozaimeatora=(TextView)convertView.findViewById(R.id.tBrojKnjiga);
        mjestozaimeknjige.setText(getItem(position).getNaziv());
        mjestozaimeatora.setText("");



        return convertView;
    }
}

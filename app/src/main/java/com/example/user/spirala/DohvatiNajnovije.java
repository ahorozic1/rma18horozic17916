package com.example.user.spirala;

import android.os.AsyncTask;

import com.example.user.spirala.Autor;
import com.example.user.spirala.Knjiga;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class DohvatiNajnovije extends AsyncTask <String, Integer, Void> {

    public interface IDohvatiNajnovijeDone {
        public void onNajnovijeDone(ArrayList<Knjiga> rezultat);

        //void onNajnovijeDone(ArrayList<Knjiga> rezultat);
    }

    private IDohvatiNajnovijeDone pozivatelj;


    ArrayList<Knjiga> rezultat = new ArrayList<Knjiga>();

    public DohvatiNajnovije(IDohvatiNajnovijeDone i) {
        pozivatelj = i;
    }

    //pet najnovijih knjiga koje je napisao autor
    @Override
    protected Void doInBackground(String... strings) {
        //zamjena svih znakova koji se ne mogu nalaziti u URL
        String query = null;
        try {
            query=URLEncoder.encode(strings[0], "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url1 = "https://www.googleapis.com/books/v1/volumes?q=inauthor:" + query + "&orderBy=newest";
        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rez = convertStreamToString(in);
            JSONObject jo = new JSONObject(rez);
            //id, naziv,autori, opis, datumobjave, slika,  brojstranica -Atributi za knjigu koji mi trebaju
            JSONArray knjige = jo.getJSONArray("items");
            for (int i = 0; i < 5; i++) {
                JSONObject knjiga = knjige.getJSONObject(i);
                String id = knjiga.getString("id");
                //t id=Integer.parseInt(idS);
                JSONObject informacije = knjiga.getJSONObject("volumeInfo");
                String naziv = informacije.getString("title");
                JSONArray autori = informacije.getJSONArray("authors");
                ArrayList<Autor> autorilista = new ArrayList<Autor>();
                //TREBAS SAD IZ JSONA U NIZ AUTORA PREBACIT NEKAKO
                if (autori != null) {
                    for (int j = 0; j < autori.length(); j++) {
                        autorilista.add(new Autor(autori.getString(j), id));
                    }
                }
                String opis = informacije.getString("description");
                String datumobjave = informacije.getString("publishedDate");
                JSONObject slike = informacije.getJSONObject("imageLinks");
                String slikaS = slike.getString("thumbnail");
                URL slika = new URL(slikaS);
                String brojstranicaS = informacije.getString("pageCount");
                int brojstranica = Integer.parseInt(brojstranicaS);
                rezultat.add(new Knjiga(id, naziv, autorilista, datumobjave, opis, slika, brojstranica));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        pozivatelj.onNajnovijeDone(rezultat);


    }


    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {

        } finally {

            try {
                is.close();
            } catch (IOException e) {

            }
        }
        return sb.toString();
    }
}


package com.example.user.spirala;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import static com.example.user.spirala.BazaOpenHelper.*;
import static com.example.user.spirala.KategorijeAkt.kategorije;

public class ListeFragment extends Fragment {
    ArrayAdapter<String> myAdapterInstance;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_liste, container, false);

    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //uzimanje referenci
        final ListView lista = (ListView) getView().findViewById(R.id.listaKategorija);
        final Button dugmePretrazi = (Button) getView().findViewById(R.id.dPretraga);
        final Button dugmeKategorija = (Button) getView().findViewById(R.id.dDodajKategoriju);
        final Button dugmeDodajKnjigu = (Button) getView().findViewById(R.id.dDodajKnjigu);
        final EditText tekst = (EditText) getView().findViewById(R.id.tekstPretraga);

        /** dodjelis listView-u listu katategorija kad klikne dugme kategorije*/
        if (getArguments() != null && getArguments().containsKey("kategorije")) {
            kategorije = getArguments().getStringArrayList("kategorije");
            int layoutID = android.R.layout.simple_list_item_1;
            myAdapterInstance = new ArrayAdapter<String>(getActivity(), layoutID, KategorijeAkt.boh.izvadiKategorije());
            //myAdapterInstance.notifyDataSetChanged();
            lista.setAdapter(myAdapterInstance);


            /**ne moze dugme dok se ne proba naci kategorija**/
            dugmeKategorija.setEnabled(false);

            /*za klik na dugme pretrazi*/
            dugmePretrazi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //filtrira da po unesenom tekstu listu kategorija
                    myAdapterInstance.getFilter().filter(tekst.getText().toString(), new Filter.FilterListener() {
                        @Override
                        public void onFilterComplete(int count) {
                            if (count == 0) {
                                dugmeKategorija.setEnabled(true);
                                dugmeKategorija.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        kategorije.add(0, tekst.getText().toString());
                                        KategorijeAkt.boh.dodajKategoriju(tekst.getText().toString());
                                        tekst.setText("");
                                        myAdapterInstance = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, kategorije);
                                        lista.setAdapter(myAdapterInstance);

                                    }
                                });
                            } else {
                                dugmeKategorija.setEnabled(false);
                            }
                        }
                    });
                }
            });
            dugmeDodajKnjigu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    android.app.FragmentManager fm = getFragmentManager();
                    DodavanjeKnjigeFragment dkf; //= (DodavanjeKnjigeFragment) fm.findFragmentById(R.id.fFragmenti);
                    // /kreiramo novi fragment FragmentDetalji ukoliko već nije kreiran
                    dkf = new DodavanjeKnjigeFragment();

                    /*Bundle argumenti = new Bundle();
                    argumenti.putStringArrayList("kategorije", kategorije);
                    lf.setArguments(argumenti);*/
                    fm.beginTransaction().replace(R.id.fFragmenti, dkf).addToBackStack(null).commit();

                }
            });
            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                    ArrayList<String> kategorije = getArguments().getStringArrayList("kategorije");
                    android.app.FragmentManager fm = getFragmentManager();
                    KnjigeFragment kf;
                    //kf = (KnjigeFragment) fm.findFragmentById(R.id.fFragmenti);
                    //provjerimo da li je fragment već kreiran
                    //if (kf == null) {
                    //kreiramo novi fragment FragmentDetalji ukoliko već nije kreiran
                    kf = new KnjigeFragment();
                    //}
                    Bundle argumenti = new Bundle();
                    argumenti.putString("poslanakategorija", KategorijeAkt.kategorije.get(position));
                    kf.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.fFragmenti, kf).addToBackStack(null).commit();


                }
            });
        }


        // da dodjelis listview-u listu autora kad klikne dugme autori
        if (getArguments() != null && getArguments().containsKey("autori")) {
            ArrayList<Autor> autori = getArguments().getParcelableArrayList("autori");
            AdapterZaAutore mojadapter;
            Button dugmeDodajKategoriju = (Button) getView().findViewById(R.id.dDodajKategoriju);
            dugmeDodajKategoriju.setVisibility(View.GONE);
            Button dugmepretraga = (Button) getView().findViewById(R.id.dPretraga);
            dugmepretraga.setVisibility(View.GONE);
            Button dugmedodajknjigu = (Button) getView().findViewById(R.id.dDodajKnjigu);
            //EditText tekst = (EditText) getView().findViewById(R.id.tekstPretraga);
            tekst.setVisibility(View.GONE);
            ListView listica = (ListView) getView().findViewById(R.id.listaKategorija);
            mojadapter = new AdapterZaAutore(getActivity(), KategorijeAkt.autori);
            listica.setAdapter(mojadapter);
            //mojadapter.notifyDataSetChanged();
            //mojadapter.notifyDataSetChanged();
            //dodavanje knjige iz liste autora
            dugmeDodajKnjigu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    android.app.FragmentManager fm = getFragmentManager();
                    DodavanjeKnjigeFragment dkf; //= (DodavanjeKnjigeFragment) fm.findFragmentById(R.id.fFragmenti);
                    // /kreiramo novi fragment FragmentDetalji ukoliko već nije kreiran
                    dkf = new DodavanjeKnjigeFragment();
                        /*Bundle argumenti = new Bundle();
                        argumenti.putStringArrayList("kategorije", kategorije);
                        lf.setArguments(argumenti);*/
                    fm.beginTransaction().replace(R.id.fFragmenti, dkf).addToBackStack(null).commit();
                }
            });

            //klik na element liste pokrece novi fragment  lista knjiga
            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                    ArrayList<Autor> autori = getArguments().getParcelableArrayList("autori");
                    android.app.FragmentManager fm = getFragmentManager();
                    KnjigeFragment kf;
                    //kf = (KnjigeFragment) fm.findFragmentById(R.id.fFragmenti);
                    //provjerimo da li je fragment već kreiran
                    //kreiramo novi fragment FragmentDetalji ukoliko već nije kreiran
                    kf = new KnjigeFragment();

                    Bundle argumenti = new Bundle();
                    argumenti.putParcelable("poslaniautor", autori.get(position));
                    kf.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.fFragmenti, kf).addToBackStack(null).commit();
                }

            });
        }

    }


}



package com.example.user.spirala;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdapterZaKnjige extends ArrayAdapter<Knjiga> {
    public interface OnChangeFragmentListener {
        void changeFragment(int position);
    }

    private Context mContext;
    private ArrayList<Knjiga> listaKnjiga;
    private OnChangeFragmentListener m_onChangeFragmentListener;


    public AdapterZaKnjige( Context context, ArrayList<Knjiga> k, OnChangeFragmentListener f){
        super(context, 0, k);
        m_onChangeFragmentListener = (OnChangeFragmentListener) f;
    }

    public AdapterZaKnjige( Context context, ArrayList<Knjiga> k){
        super(context, 0, k);
        m_onChangeFragmentListener=null;
    }



    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        //View v=View.inflate(mContext, R.layout.element_liste, null);

        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.element_liste, viewGroup, false);
        }


        TextView tekstzanaziv = (TextView) view.findViewById(R.id.eNaziv);
        TextView tekstzaautora = (TextView) view.findViewById(R.id.eAutor);
        ImageView zasliku = (ImageView) view.findViewById(R.id.eNaslovna);
        TextView tekstzaopis = (TextView) view.findViewById(R.id.eOpis);
        TextView tekstzadatum = (TextView) view.findViewById(R.id.eDatumObjavljivanja);
        TextView tekstzabrojstr = (TextView) view.findViewById(R.id.eBrojStranica);
        Button dugmePreporuci = (Button) view.findViewById(R.id.dPreporuci);

        //postavljanje

        //provjera je li obojena, ista za oboje
        if (getItem(i).isDaLiJeObojena()) {
            view.setBackgroundResource(R.color.bojaZaElementLise);
        }

        //Postavljanje naziva knjige, isto za oba nacina dodavanja
        tekstzanaziv.setText(getItem(i).getNaziv());


        //Postavljanje imena autora
        tekstzaautora.setText(getItem(i).getImeAutora());






        //POSTAVLJANJE SLIKE
        if(getItem(i).getDodanaOnline()){
            //ako je dodana online
            if(getItem(i).getSlika()!=null) {
                    Picasso.get().load(getItem(i).getSlika().toString()).into(zasliku);
            } else{
                    zasliku.setImageBitmap(null);
            }
        }else {
            //ako nije dodana
            if(getItem(i).getNaslovnaStrana()!=null) {

                try {

                    zasliku.setImageBitmap(BitmapFactory.decodeStream(getContext().openFileInput(getItem(i).getNaslovnaStrana())));
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(DodavanjeKnjigeAKt.class.getName()).log(Level.SEVERE, null, ex);
                }
                catch (Exception ex){

                }
            }

        }

        //Postavljanje novih atributa

        if(getItem(i).getDodanaOnline()){
            tekstzabrojstr.setText(Integer.toString(getItem(i).getBrojStranica()));
            tekstzaopis.setText(getItem(i).getOpis());
            tekstzadatum.setText(getItem(i).getDatumObjavljivanja());
        }else {
            tekstzaautora.setText("");
            tekstzabrojstr.setText("");
            tekstzadatum.setText("");
        }
        dugmePreporuci.setTag(i);


        dugmePreporuci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_onChangeFragmentListener.changeFragment((Integer)view.getTag());
            }
        });



       /* if(getItem(i).getSlika()!=null) {
            try {

                zasliku.setImageBitmap(BitmapFactory.decodeStream(getContext().openFileInput(getItem(i).getNaslovnaStrana())));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DodavanjeKnjigeAKt.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch (Exception ex){

            }
        }*/

      // v.setTag(listaKnjiga.get(i).getId());


        return view;
    }



}

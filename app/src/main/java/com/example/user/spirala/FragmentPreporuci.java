package com.example.user.spirala;

import android.Manifest;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

public class FragmentPreporuci extends Fragment {

    final static int PERMISSION_REQUEST=1;




    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_preporuci, container, false);

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        Knjiga poslanaknjiga =null;
        poslanaknjiga= getArguments().getParcelable("knjiga");
        //kontakti = new ArrayList<Kontakt>();


        //UZIMANJE REFERENCI

        final TextView naziv = (TextView) getView().findViewById(R.id.tNaziv);
        final TextView autor = (TextView) getView().findViewById(R.id.tAutor);
        TextView brojstranica = (TextView) getView().findViewById(R.id.tBrojStranica);
        TextView datumobjavljivanja = (TextView) getView().findViewById(R.id.tDatumObjavljivanja);
        TextView opis = (TextView) getView().findViewById(R.id.tOpis);
        final Spinner skontakti = (Spinner) getView().findViewById(R.id.sKontakti);
        Button dugmeposalji=(Button)getView().findViewById(R.id.dPosalji);


        if (poslanaknjiga.getDodanaOnline()) {

            naziv.setText("Naziv: " + poslanaknjiga.getNaziv());

            try {
                ArrayList<String> imenaautora = new ArrayList<String>();
                //Pokupis imena svih autora
                for (Autor x : poslanaknjiga.getAutori()) {
                    imenaautora.add(x.getImeiPrezime());
                }
                String sviautori = TextUtils.join(" ", imenaautora);

                autor.setText("Autori: " + sviautori);
            }catch(Exception e){}

            try {
                brojstranica.setText("Broj stranica: " + Integer.toString(poslanaknjiga.getBrojStranica()));
            }catch (Exception e){}


            try {
                datumobjavljivanja.setText("Datum objavljivanja" + poslanaknjiga.getDatumObjavljivanja());
            }catch(Exception e){}

            try{
                opis.setText("Opis: " + poslanaknjiga.getOpis());
            }catch(Exception e){}

        } else {
            naziv.setText("Naziv: " + poslanaknjiga.getNaziv());
            autor.setText("Autor: " + poslanaknjiga.getImeAutora());

        }

        //Kontakti
        DohvatiKontakte();
        ArrayList<String> kontaktiime=new ArrayList<String>();
        for(Kontakt x: KategorijeAkt.kontakti){
            kontaktiime.add(x.getIme());
        }
        ArrayAdapter<String> ad = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kontaktiime );
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        skontakti.setAdapter(ad);


        dugmeposalji.setOnClickListener(new View.OnClickListener() {
            public Intent chooser;

            @Override
            public void onClick(View view) {
                Intent intent= new Intent(Intent.ACTION_SEND);
                intent.setData(Uri.parse("mailto: "));
                int pozicija=skontakti.getSelectedItemPosition();
                String [] mailkomesaljes={KategorijeAkt.kontakti.get(pozicija).getEmail()};
                intent.putExtra(Intent.EXTRA_EMAIL, mailkomesaljes);
                intent.putExtra(Intent.EXTRA_SUBJECT,"Preporuka knjige");
                intent.putExtra(Intent.EXTRA_TEXT, "Zdravo " + KategorijeAkt.kontakti.get(pozicija).getIme()+ ", \nPročitaj knjigu " + naziv.getText().toString() + " od " + autor.getText().toString() + "!");
                intent.setType("message/rfc822");
                chooser= Intent.createChooser(intent, "Send Email");
                startActivity(chooser);
            }
        });



    }

    void DohvatiKontakte(){

        boolean imalidozvolu=Dozvola();

        Cursor kursorzakontakte = null;
        ContentResolver cr = getActivity().getContentResolver();
        // KONTAKTE UZIMAS
        if(imalidozvolu) {
            try {
                kursorzakontakte = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
                if (kursorzakontakte != null && kursorzakontakte.getCount() > 0) {
                    while (kursorzakontakte.moveToNext()) {
                        String id = kursorzakontakte.getString(kursorzakontakte.getColumnIndex(ContactsContract.Contacts._ID));
                        Cursor kursorzamail = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                        while (kursorzamail.moveToNext()) {
                            String ime = kursorzamail.getString(kursorzamail.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            String email = kursorzamail.getString(kursorzamail.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                            if (email != null) {
                                KategorijeAkt.kontakti.add(new Kontakt(ime, email));

                            }


                        }
                        kursorzamail.close();
                    }
                }
            } catch (Exception e) {
                // Here, thisActivity is the current activity


            }
        }



    }

    boolean Dozvola(){
        boolean ima=false;
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            //Nema dozvole
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_CONTACTS)) {
                ima=false;
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_CONTACTS},
                        PERMISSION_REQUEST);

                ima=true;
            }
        } else {
            ima=true;
        }
        return ima;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0  && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    DohvatiKontakte();
                } else {
                   getFragmentManager().popBackStack();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

}

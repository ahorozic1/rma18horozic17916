package com.example.user.spirala;

import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by User on 27. 3. 2018..
 */

public class Knjiga implements Parcelable{

    private String id;
    private String naziv;
    private ArrayList<Autor> autori;
    private String opis;
    private String datumObjavljivanja;
    private URL slika;
    private int brojStranica;
    private boolean daLiJeObojena;
    private String naslovnaStrana;
    private String kategorija;



    private boolean dodanaOnline;

    public boolean getDodanaOnline() {
        return dodanaOnline;
    }
    public void setDodanaOnline(boolean dodanaOnline) {
        this.dodanaOnline = dodanaOnline;
    }


    //stari atribut
    private  String imeAutora;

    protected Knjiga(Parcel in) {
        id = in.readString();
        naziv = in.readString();
        autori = in.createTypedArrayList(Autor.CREATOR);
        opis = in.readString();
        datumObjavljivanja = in.readString();
        brojStranica = in.readInt();
        daLiJeObojena = in.readByte() != 0;
        naslovnaStrana = in.readString();
        kategorija = in.readString();
        imeAutora = in.readString();
    }

    public static final Creator<Knjiga> CREATOR = new Creator<Knjiga>() {
        @Override
        public Knjiga createFromParcel(Parcel in) {
            return new Knjiga(in);
        }

        @Override
        public Knjiga[] newArray(int size) {
            return new Knjiga[size];
        }
    };

    public String getImeAutora() {
        return imeAutora;
    }

    public void setImeAutora(String imeAutora) {
        this.imeAutora = imeAutora;
    }

    //trazeni konstruktor
    public Knjiga(String id, String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika, int brojStranica) {
        this.id = id;
        this.naziv = naziv;
        this.autori = autori;
        this.opis = opis;
        this.datumObjavljivanja = datumObjavljivanja;
        this.slika = slika;
        this.brojStranica = brojStranica;
        this.dodanaOnline=true;
        imeAutora="";

        for (Autor a: autori
                ) {
            imeAutora= imeAutora.concat(a.getImeiPrezime()+" ");
        }
    }

    // stari konstruktor
    public Knjiga(String naslov, String naslovnaStrana,String nazivAutora, String kategorija) {
        this.naziv= naslov;
        this.naslovnaStrana = naslovnaStrana;
        this.imeAutora = nazivAutora;
        this.kategorija = kategorija;
        this.dodanaOnline=false;
    }




    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<Autor> getAutori() {
        return autori;
    }

    public void setAutori(ArrayList<Autor> autori) {
        this.autori = autori;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getDatumObjavljivanja() {
        return datumObjavljivanja;
    }

    public void setDatumObjavljivanja(String datumObjavljivanja) {
        this.datumObjavljivanja = datumObjavljivanja;
    }

    public URL getSlika() {
        return slika;
    }

    public void setSlika(URL slika) {
        this.slika = slika;
    }

    public int getBrojStranica() {
        return brojStranica;
    }

    public void setBrojStranica(int brojStranica) {
        this.brojStranica = brojStranica;
    }

    public boolean isDaLiJeObojena() {
        return daLiJeObojena;
    }

    public void setDaLiJeObojena(boolean daLiJeObojena) {
        this.daLiJeObojena = daLiJeObojena;
    }

    public String getNaslovnaStrana() {
        return naslovnaStrana;
    }

    public void setNaslovnaStrana(String naslovnaStrana) {
        this.naslovnaStrana = naslovnaStrana;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(naziv);
        parcel.writeTypedList(autori);
        parcel.writeString(opis);
        parcel.writeString(datumObjavljivanja);
        parcel.writeInt(brojStranica);
        parcel.writeByte((byte) (daLiJeObojena ? 1 : 0));
        parcel.writeString(naslovnaStrana);
        parcel.writeString(kategorija);
        parcel.writeString(imeAutora);
    }
}
